
# Changelog

## 2024 April release (24.04)

- Provide a clear positioning of Gaia-X Trust Framework in the context of other (especially DSBA) data space initiatives
- New view on how the Trust Framework can be used to create domain and ecosystem specific extensions
- Generic Trust Framework process flow and roles for CABS fully supporting the Label definitions in the PRCD
- Introducing Signature and Party credentials (which are defined in the ICAM document)
- Add Protocol, Standards and API into the Gaia-X Services chapter
- Link to the new Technical Specification "Software Architecture"
- Move Credential Event Service to the Gaia-X Services chapter and provide functional specification details
- Clarifications and more precise wording in the Data Exchanges Services chapter based on reader feedback
- Detailed description on the use of policy engines and the link between ODRL and VC
- Added Annex for updated Trust Indexes
- Clarification on Data Usage Agreements as generalisation of Consent (GDPR) and Permission (Data Act)

## 2023 October release (23.10)

- Replace "Overview", which included general concepts, with "Context", which defines the specific scope of the Gaia-X Architecture
- Update and generalization of the "Conceptual Model"
- Generic description of "Policy Management"
- Mapping of Gaia-X Architecture to the CASCO model
- Updates based on changes in the Data Exchange Services and Identity, Credential and Access Management Document
- Introduction of the "Party-Credential" as extension of Identity and Service Offering credentials with Membership and Service credentials
- Contains the technical implementation details for the Trust Framework (from the 22.10 Trust Framework document)
- Annex includes a section on "Gaia-X Digital Clearing House"
- Chapter "Gaia-X Trust Framework components" including "Gaia-X Compliance", "Gaia-X Registry", and "Gaia-X Notary - LRN"
- Inclusion of the "Publication/Subscription service" (ref. Federated Catalogues) and "Trust Indexes" sections in the new chapter "Enabling and Federation Services"  

## 2022 October release (22.10)

- Update the chapter on Service Composition
- Add a section on Data Mesh

## 2022 September release (22.09)

- Move Self-Description technical specs to the next Identity, Credential and Access Management document
- Update Self-Description lifecycle status
- Introduction of Interconnection Point Identifiers
- Introduction of new language terms for Data Exchange
- Update of the Gaia-X schemas and diagrams aligned with the [Gaia-X Framework](https://docs.gaia-x.eu/framework/)

## 2022 April release (22.04)

- Link to Trust Framework document (where the Self-Description mandatory attributes now are)
- Aligning Gaia-X architecture with NIST Cloud Federation Reference Architecture (CFRA)
- Updated definition of Data Exchange Services
- Updated Service Composition and Resource model
- Updated Self-Description Lifecycle
- Consistency and alignment with other officially published Gaia-X documents, streamlining and de-duplication of text to ease reading

## 2021 December release (21.12)

- Adding `Contract` and `Computable Contract` definitions in the Conceptual Model
- Update on the Self-Description lifecycle management
- Update on the Federated Trust Model

## 2021 September release (21.09)

- Rewrite the Operating model chapter introducing Trust Anchors, Gaia-X Compliance, Gaia-X Labels and Gaia-X Registry.
- Update of Self-Description mandatory attributes in the Appendix.
- Update of `Interconnection`, `Resource` and `Resource template` definitions.
- Gitlab automation improvement and speed-up
- Source available in the [21.09](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-architecture-document/-/tree/21.09) branch.

## 2021 June release (21.06)

- Adding a new Operating model section introducing the first principle for Gaia-X governance.
- Adding preview of Self-Description mandatory attributes in the Appendix.
- Improvement of the Policy rules.
- Improvement of the `Asset` and `Resource` definitions.
- Complete release automation from Gitlab.
- Source available under the [21.06](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/-/tree/21.06) tag.

## 2021 March release (21.03)

- First release of the Architecture document by the [Gaia-X Association AISBL](https://www.gaia-x.eu/)
- Complete rework of the Gaia-X Conceptual Model with new entities' definition.
- Adding a Glossary section.
- Source available under the [21.03-markdown](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/-/tree/21.03-markdown) tag.

## 2020 June release (20.06)

- First release of the Technical Architecture document by the [BMWi](https://www.bmwi.de/Navigation/DE/Home/home.html)
