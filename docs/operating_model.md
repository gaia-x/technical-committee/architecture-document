# Operating Models

This section describes the link between the documented policy rules to which participants of an Ecosystem agree (for Gaia-X defined in the [Policy Rules Compliance Document](https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/), Domains or Ecosystems may extend with own policy rules) and the implementation through the software components and processes defined in the technical architecture, described in this document, and associated specifications.

Gaia-X in its unique endeavour must have an operating model enabling a widespread adoption by small and medium-sized enterprises up to large organisations, including those in highly regulated markets, to be sustainable and scalable.

To achieve the objectives above, a non-exhaustive list of Critical Success Factors (CSFs) includes these points:

1. The operating model must provide clear and unambiguous added value to all Participants
2. The operating model must have a transparent governance and trust model with identified accountability and liability, that is clearly and fully explained to all Participants
3. The operating model must be easy to use by all Participants
4. The operating model must be financially sustainable for the Gaia-X Ecosystem
5. The operating model must be environmentally sustainable.

## Context

Gaia-X provides a mechanism to translate Policy Rules as defined in regulatory, standards or commercial documents into criterial which can be digitally validated by a set of "Trust Anchors" which each ecosystem can define. This chapter describes the components an processed that operationalize this process.

![Overview of Conformity Framework](figures/Gaia-X_Conformity_Schema.png)

### Prerequisites

To achieve the above goals, it is recommended for the readers to be familiar with the two documents below:

- [ISO/IEC 17000:2020 Conformity assessment](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en) definitions prepared by the ISO Committee on Conformity Assessment [CASCO](https://www.iso.org/casco.html)
- [Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/) definitions from the World Wide Web Consortium (W3C)

!!! note

    Versions 22.11 and below of Gaia-X documents are referring to Verifiable Credentials Data Model 1.1. It is expected to adopt Verifiable Credentials Data Model 2.0 in later releases.

The two mentioned documents address different types of readers, and the following sections will establish links between the various terms used in those documents.

#### CASCO workflows

There is an overarching conformity system including one or more conformity schemes.

```mermaid
flowchart

    SystemOwner(System Owner)

    subgraph CASystem [Conformity Assessment System]
        direction TB
        SchemeOwner(Schema Owner)
        CAScheme(Conformity Assessment Scheme)
        SchemeOwner -- develops and maintains --> CAScheme
    end

    SystemOwner -- develops and maintains --> CASystem
```

### Scheme model

```mermaid
flowchart

    CAScheme(Conformity Assessment Schemes)
    criteria(Specified Requirements)
    AB(Accreditation Bodies)
    CAA(Conformity Assessment Activities)
    CAB(Conformity Assessment Bodies)
    object(Objects of Conformity Assessment)
    SchemeOwner(Schema Owners)

    CAA -- demonstrate fulfilment of --> criteria
    criteria -- need or expectation applying to --> object
    CAB -- perform --> CAA
    AB -- issue accreditations to --> CAB
    CAScheme -- identify --> criteria
    CAScheme -- provide the methodology to perform --> CAA
    SchemeOwner -. award authority to .->  AB
    SchemeOwner -- develop and maintain --> CAScheme
```

#### Roles and mapping between CASCO and Verifiable Credential Data Model

The ISO/IEC 17000:2020 Conformity assessment and Verifiable Credentials Data Model documents mentioned above have different but overlapping scopes. To ease the analysis, two roles must be distinguished:

1. the body or party making a claim or attestation about an object.
2. the body or party cryptographically signing a claim or attestation.

While it is commonly expected that those two roles would be endorsed by the same identifiable legal or natural participant, it's not always the case, especially when the **CAB** is not capable of performing digital signatures or when the [**credentialSubject**](https://www.w3.org/TR/vc-data-model/#credential-subject) contains several `<subject> <predicate> <object>` triples as defined in [Resource Description Framework (RDF)](https://www.w3.org/TR/rdf11-primer/).

!!! note

    `<subject> <predicate> <object>` triples in [RDF](https://www.w3.org/TR/rdf11-primer/) are also called `<subject>-<property>-<value>` [claims](https://www.w3.org/TR/vc-data-model/#claims) in the Verifiable Credential Data Model.

| Scenario                                                              | ISO/IEC 17000:2020 term                                                         | Verifiable Credentials Data Model term                                       |
|-----------------------------------------------------------------------|---------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| 1. the object being described.                                        | [object](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.2) | [credentialSubject](https://www.w3.org/TR/vc-data-model/#credential-subject) ([RDF object](https://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#dfn-object)) |
| 2. the body or party making a claim or attestation about an object.   | [CAB](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6)    | [credentialSubject](https://www.w3.org/TR/vc-data-model/#credential-subject) ([RDF subject](https://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#dfn-subject))|
| 3. the body or party cryptographically signing a claim or attestation.| [CAB](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6)    | [issuer](https://www.w3.org/TR/vc-data-model/#issuer)                        |

#### Gaia-X Credentials and Attestations

As seen in the previous section, an object can be described by bodies or parties having different relations with the object itself.

An `object` can be any entity from the Gaia-X information models, like, and not limited to, a [Service Offering](component_details.md#resources-and-service-offerings), a [Data Product](component_details.md#data-products-and-data-exchange-services), a [Participant](gx_conceptual_model.md#participant), and a [Data Usage Agreement](component_details.md#data-license-and-data-usage-agreement).
Credentials subjects can also declare delegation of authority (see: Trust Anchor, Party and Signature credentials as examples defined in the [ICAM document](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/24.07/)) which can be used to implement the "Power of Attorney" concept defined in the [Gaia-X Compliance Document](https://docs.gaia-x.eu/policy-rules-committee/compliance-document/24.06/).

A Gaia-X credential is a Verifiable Credential (VC) using the Gaia-X Ontology which is available via the Gaia-X Registry.
A holder can put several Gaia-X credentials together to build a [Verifiable Presentation (VP)](https://www.w3.org/TR/vc-data-model-2.0/#presentations).

This simple approach is not enough and below there is a mapping between a newer vocabulary used in the Gaia-X documents and [ISO/IEC 17000:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en) definitions prepared by the ISO Committee on Conformity Assessment [CASCO](https://www.iso.org/casco.html).

<table>
<thead>
  <tr>
    <th>ISO/IEC 17000:2020</th>
    <th>Type of <a href="https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.3">Attestion</a></th>
    <th>Example</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td><a href="https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.3">first-party conformity assessment activity</a></td>
    <td><a href="https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.5">declaration</a><br></td>
    <td>a person self-declaring itself competent</td>
  </tr>
  <tr>
    <td><a href="https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.4">second-party conformity assessment activity</a></td>
    <td></td>
    <td>assessment of a person's knowledge and skills conducted by a trainer/instructor</td>
  </tr>
  <tr>
    <td><a href="https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.5">third-party conformity assessment activity</a></td>
    <td><a href="https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.6">certification</a></td>
    <td>assessment of a person's knowledge and skills conducted with a national exam</td>
  </tr>
</tbody>
</table>

To be noted that all the terms above can be generically referred as [attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.3) and all **attestations** are issued by [**conformity assessment body (CAB)**](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6), including declarations.

#### Gaia-X Compliance schemes

The Gaia-X Conformity and Gaia-X Labels are implementations of the Gaia-X governance.

To operationalise them, a software implementation must be executed and turned into up-and-running services, providing traceable evidence of correct executions.

While the governance of the Gaia-X Compliance rules and process is and will stay under the control of the Gaia-X Association, the Gaia-X Compliance Service will go through several mutually non-exclusive deployment scenarios[^article-compliance-deployment].

[^article-compliance-deployment]: <https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/>

During the pilot phase, the current, non-final deployment is described below.

| Deliverables           | Notes |
|---------------------|-------|
| [Gaia-X Compliance Service](https://compliance.gaia-x.eu)     | This service validates the shape, content and signature of Gaia-X Credentials and issues back a Gaia-X Credential attesting of the result. Required fields and consistency rules are defined in this document. Format of the input and output are defined in the [Identity, Credential and Access management document](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/latest).|
| [Gaia-X Registry](https://registry.gaia-x.eu) | This service provides a list of valid shapes and valid and revoked public keys. The Gaia-X Registry will also be used as the seeding list for the network of catalogues. |

### Extension by the ecosystems

An ecosystem can extend the definition of the Gaia-X Trust Anchors, Gaia-X Trusted Data Sources and Gaia-X Notaries as follows:

- the ecosystem governance can add more requirements on the eligibility of the Gaia-X Trust Anchors, hence selecting a subset of the Gaia-X Trust Anchors for the ecosystem domain.
- an ecosystem governance can add additional rules on top of the ones in this document by:
    - adding more criteria for a credential type. Example: adding requirements for a participant to join the federation, enforcing a deeper level of transparency for a Service Offering
    - selecting new domain-specific Trust Anchors for the new criteria.

**! warning!**: For already defined criteria, it will break Gaia-X Compliance to extend the list of Gaia-X Trust Anchors eligible to sign the criteria.

![Ecosystem rules](figures/ecosystem_rules.svg)

$$\mathbb{R}_{GaiaX} \subseteq \mathbb{R}_{domain}$$

| Rule property                                 | A domain refining Gaia-X Conformity <br>$\forall r \in \mathbb{R}_{GaiaX} \text{ and } r_{domain} \in \mathbb{R}_{domain}$ | A domain extending Gaia-X Conformity<br>$\forall r \in \mathbb{R}_{domain} \setminus \mathbb{R}_{GaiaX}$ |
|-----------------------------------------------|--------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| Attribute name: $r_{name}$                           | $r_{name_{CamelCase}} \equiv r_{name_{snake\_case}}$                               | $r_{name} \notin \mathbb{R}_{GaiaX}$                             |
| Cardinality: $\lvert r \lvert$               | $\lvert r_{domain} \lvert \ge \lvert r \lvert$                                     | _no restriction_                                                   |
| Value formats: $r \rightarrow \texttt{VF}(r)$          | $\texttt{VF}(r_{domain}) \subseteq \texttt{VF}(r)$                                       | _no restriction_                                                   |
| Trust Anchors: $r \rightarrow \texttt{TA}(r)$         | $\texttt{TA}(r_{domain}) \subseteq \texttt{TA}(r)$                             | _no restriction_                                                   |
| Trusted Data Sources: $r \rightarrow \texttt{TDS}(r)$ | $\texttt{TDS}(r_{domain}) \subseteq \texttt{TDS}(r)$                 | _no restriction_                                                   |

#### Compliance remediation

Gaia-X Compliance credentials may become invalid over time. There are three states declaring such a credential as invalid:

- Expired (after a timeout date, e.g., the expiry of a cryptographic signature)
- Deprecated (replaced by a newer Gaia-X Credential)
- Revoked (by the original issuer or a trusted party, e.g., because it contained incorrect or fraudulent information)

Expired and Deprecated states can be deduced automatically based on the information already stored in the Gaia-X Registry or Gaia-X Catalogues. There are no additional processes to define. This section describes how Gaia-X Credentials are revoked.

The importance of Gaia-X Compliance will grow over time, covering more and more Gaia-X principles such as interoperability, portability, and security. However, automation alone is not enough, and the operating model must include a mechanism to demotivate malicious actors to corrupt the Gaia-X Registry and Gaia-X Catalogues.

The revocation of Gaia-X credentials can be done in various ways:

- **Revocation or Deprecation by authorship**: The author of a Gaia-X credential revokes or deprecates the credential explicitly.
- **Revocation by automation**: The Gaia-X Compliance Service found at least one credential claim not validating the Gaia-X conformity rules.
- **Suspension and Revocation by manual decision**: After an audit by a compliant Gaia-X Participant, if at least one credential claim is found to be incorrect, the suspension of the Gaia-X credential is automatic. The revocation is submitted for approval to the Gaia-X Association with the opportunity for the credential issuer to state its views in a matter of days. To minimize subjective decisions and promote transparency, the voting results will be visible and stored on the Gaia-X Registry or in the local Ecosystem's Registry.

### Gaia-X Compliance Schema and Process

The following graph describes the generic compliance schema which is applied to Gaia-X conformance, labels and can be used for domain and ecosystem specific extensions:

![Overview of Compliance Framework](figures/Gaia-X_Conformity_Schema_process.png)

The different types of Conformity Assessment Bodies are specified in the PRCD document.


## Gaia-X Decentralized Autonomous Ecosystem

The operating model described in this chapter motivates the creation of a Gaia-X decentralized autonomous Ecosystem following the principles of a Decentralized Autonomous Organisation[^dao], with the following characteristics:

- Compliance is achieved through a set of automatically enforceable rules whose goal is to incentivize its community members to achieve a shared common mission.
- Maximizing the decentralization at all levels to reduce lock-in and lock-out effects.
- Minimizing the governance and central leadership to minimize liability exposure and regulatory capture.
- The ecosystem has its own rules, including management of its own funds.
- The ecosystem is operated by the ecosystem's Participants

[^dao]: Example of the setup of a DAO <https://blockchainhub.net/dao-decentralized-autonomous-organization/>

:information_source: Other ecosystems are autonomous and this operating model does not enforce how internal ecosystem governance should be handled.

## Data Usage operating model

The overall Data Usage process in Gaia-X is voluntarily independent from technical means deployed by the Data Provider to enable data usage: it can be a on-off data transfer after an API call, it can be a continuous stream of data, it can be a call back each time the data changes, it can be the execution of the Consumer provided function in a provider environment (hence the Consumer never sees the data and just gets the result), etc.
Accordingly, the possible technical means enabling Data Usage are not further detailed here.
 
The most unique aspect of Gaia-X Data Usage process is related to the Data Usage Agreement signature. 
Several cases are to be considered, depending on the kind of data license and on the relationship between the Data Rights Holder, the Data Provider and the Data Consumer:
-	Case 1: generic license, where the Data Rights Holder does not want to know who is using its data (open-data is a specific case of generic license with a DUA signed by the Data Rights Holder giving everybody un-constrained usage of the data),
-	Case 2a: specific license where the Data Consumer has access to the Data Rights Holder (this is usually the case when the Data Consumer provides a service to the Data Rights Holder using the Data Rights Holder’s data, for instance when a sports training app uses historical monitoring data from a sports watch provider to provider sport coaching advice, and
-	Case 2b: specific license where the Data Provider has access to the Data Rights Holder, but the Data Consumer (for instance a Data Consumer wanting to use data for statistical purposes) does not have access.

The  basic operating model for data usage with a generic license is quite simple:

![Data Usage Operating model 1](figures/Gaia-X_models_Data_Product_Operational_Case1.svg)

*Figure 5.1 - Data usage operating model – case 1*

1. The Data Rights Holder signs a generic DUA and notarizes it through a DUA Trust Anchor. It then communicates the signed DUA to the Data Provider who stores it in the Data Product Description within the Catalogue (it replaces the generic Data License in that case).
2.	The Data Consumer queries the Data Product Catalogue and reviews the Data Product Descriptions (including the Data License) to select a Data Product that corresponds to its needs.
3.	The Data Consumer configures the Data Product in terms of data scope and operational characteristics and starts negotiating with the Data Provider.
4.	When Data Consumer and Data Provider find an agreement, they sign a configured Data Product Description to create the Data Usage Contract (DUC) and can optionally notarize it in a Federated Data Usage Contract Store.
5.	The Data Consumer countersigns the DUA (retrieved from the Data Product Description) and notarizes it through the DUA Trust Anchor.
6.	The Data Consumer requests Data Access providing the DUA. The Data Provider checks the DUA's validity, applicability and status. If everything is OK, it activates the instantiated Data Product resulting in actual Data Access enabling Data Usage.



The operating model is a bit more complex when the Data Product includes specific licensed data. 


If the Data Consumer has access to the Data Rights Holder (case 2a below), then it can directly request the Data Usage Agreement – this is usually the case when the Data Consumer is using the data to provide a service to the Data Rights Holder (for instance when a sports training app get historical monitoring data from a sports watch provider to provide sports coaching) or when the Data Rights Holder accepts that its contact points are communicated to the Data Consumer by the Data Provider. Otherwise, the Data Usage Agreement has to be collected by the Data Provider.


![Data Usage Operating model 2a](figures/Gaia-X_models_Data_Product_Operational_Case2a.svg)

*Figure 5.2 - Data usage operating model – case 2a*

1.	The Data Consumer queries the Data Product Catalogue and reviews the Data Product Descriptions (including the Data License) to select a Data Product that corresponds to its needs.
2.	The Data Consumer configures the Data Product in terms of data scope and operational characteristics and starts the negotiation with the Data Provider.
3.	The Data Provider and the Data Consumer close the negotiation, and they sign the configured Data Product Description to create the Data Usage Contract (DUC). They can optionally notarize the Data Usage Contract in a Federated DUC Store.
4.	The Data Consumer extracts the Data Usage Agreement template from the Data Product license, fills it with its specific information (its identity, how the data will be used, for which purpose, …), and communicates it to the Data Rights Holder who signs it, notarizes it through a Data Usage Agreement Trust Anchor and gives it back to the Data Consumer. The Data Consumer checks the DUA validity (i.e. checks that the Data Right Holder is really holding the rights on that data), countersigns it and notarizes it through the DUA Trust Anchor.
5.	The Data Consumer requests Data Access providing the DUA. The Data Provider checks the DUA validity, applicability and status. If everything is OK, it activates the instantiated Data Product resulting in actual Data Access enabling Data Usage.


If the Data Consumer has no access to the Data Rights Holder (case 2b below), then the operating model is similar except for step 4, where the Data Usage Agreement is requested through the Data Provider (directly or through the Data Producer). 

![Data Usage  Operating model 2b](figures/Gaia-X_models_Data_Product_Operational_Case2b.svg)

*Figure 5.3 - Data usage operating model – case 2b*

1.	The Data Consumer queries the Data Product Catalogue and reviews the Data Product Descriptions (including the Data License) to select a Data Product that corresponds to its needs.
2.	The Data Consumer configures the Data Product in terms of data scope and operational characteristics and starts the negotiation with the Data Provider.
3.	The Data Provider and the Data Consumer close the negotiation, and they sign the configured Data Product Description to create the Data Usage Contract (DUC). They can optionally notarize this Data Usage Contract in a Federated DUC Store.
4.	The Data Consumer extracts the Data Usage Agreement template from the Data Product license, and fills it with its specific information (its identity, how the data will be used, for which purpose, …). The Data Consumer communicates this Data Usage Agreement to the Data Rights Holder through the Data Provider (possibly through the Data Producer in some cases). The Data Rights Holder signs it, notarizes it through a Data Usage Agreement Trust Anchor and gives it back to the Data Consumer, through the Data Provider. The Data Consumer checks the DUA validity, countersigns it and notarizes it through the DUA Trust Anchor.
5.	The Data Consumer requests Data Access providing the DUA. The Data Provider checks the DUA's validity, applicability and status. If everything is OK, it activates the instantiated Data Product resulting in Data Access enabling actual Data Usage.

!!!NOTE: In fact there is no need to transfer the real DUA back and forth - transferring the DUA Universal Identifier is enough. The DUA content is stored in the DUA Trust Anchor and communicated on demand. With this mechanism, it is possible to communicate to the various actors only the parts of the DUA which are relevant to them. For instance, the Data Provider does not need to have access to the purpose of the data usage. 

!!!NOTE: Requesting the Data Rights Holder to sign the DUA first enables the Data Consumer to check the validity of the DUA (i.e. check that the Data Right Holder is really holding the rights on that data) before countersigning it and starting data usage. This will usually be requested by the control-structures of Data Consumer to get evidence that the data usage will be legal. 

!!!NOTE: DUA Trust Anchors might provide additional services to check DUA validity and applicability. This is not mandated by Gaia-X but this would provide convenient services to the various actors, especially for data usage across ecosystems as such checks can be domain-dependent and be complex to execute in some ecosystems. DUA Trust Anchors can also provide additional services like analysis of usage licenses and usage purposes for Data Consumer, DUA statistics for Data Consumer control teams, DUA dashboard and data usage statistics for Data Rights Holders (e.g. signed DUAs which are not used anymore, …).

<!-- I propose to remove this part as lighthouse projects have different operating model for Data intermediaries
### Data Intermediary generic operating model

Several Lighthouse projects implement the concept of Data Intermediary. Compared to the general Gaia-X Conceptual Model, a Data Intermediary is an actor who plays several roles: Data Product Provider acting as a proxy between the Data Producer and the Data Consumer, Provider operating a Data Product Catalogue (often named data marketplace), Data Usage Agreement Trust Anchor acting as a trusted proxy between the Licensor and the Data Consumer.

The generic operating model for the Data Intermediary role is as follows:

![Data Intermediary](figures/Gaia-X_models_Data_Intermediary_Operational.svg)

*Figure 5.4 - Data Intermediary generic operating model*

Many variants of this generic model can be defined. For instance, a general Data Usage Agreement may be proposed to the Data Licensor upfront when the Data Product is inserted in the Catalogue and the Licensor can trust the Data Intermediary to check the conditions and purpose before delivering data usage to a consumer.

Hence, the above model is given as a generic example. The ecosystems are free to define and implement the operating model adapted to their specificities.
-->

## Service Composition

To pursue the description of the service composition model, we focus on the steps required to implement service composition and describe the functions required to achieve end-to-end service composition across multiple service providers. Figure 5.5 depicts these steps and starts with a service request from the user (end user, tenant, information system manager) request.

![Service Composition Activity Diagram](figures/Fig4_service_composition_-steps-activation-diagram-October-11-2022-Activation_diagram_-service_composition-steps.png)
*Figure 5.5. Gaia-X Compliant Service Composition Steps*

The user first describes the desired service using a domain-specific description language, generally JSON and YAML-based. This request is then parsed for specific end-user requirements extraction and added (concatenated) to a generic and high-level catalogue that contains service descriptions as part of a Catalogue and holds only functional descriptions of services and applications relevant to the user request, to ensure interoperability, compatibility, and portability requirements. These services are not localized a priori; location is to be set at a later stage in the service composition process. The same holds for some additional service attribute values, in some parts of the service description. These values are empty fields or wild cards that will only take on specific values once the user service request has been parsed for preferences, constraints and specific limitations (such as provider type, restrictions on localization and geographical area, unconstrained naming and addressing spaces and more). Some attribute values are only set (or resolved) and known at later stages of the service composition (as late as steps 4 and 5).

The user-expressed constraints, in step 1, are added to the functional description in step 2 to produce the actual service request that will be used to derive in a third step an extended representation of the service request. This extended representation, usually in the form of a tree (or complex service graph) representation and description of the extended service, now contains required services and resources as well as generic hosting nodes (not yet fully specified) needed to fulfil the service request. All properties and requirements from the initial service request therefore propagate to these tree nodes and leaves. This tree contains the relationships and dependencies of all services involved in building a solution to the initial user request and has as mentioned inherited all the service properties, constraints, restrictions, and obligations stated by the user in step 1. The fourth step is service discovery which will search in multiple catalogues.

The output of the fourth step produces, with the help of orchestrators and service composition, workflows and deployment plan production engines (external or even taken from the Gaia-X service offerings) to deploy the service instances and ensure their interconnection. The result is the production of a composite service graph that fulfils the initial end user request and its concrete instantiation in multiple hosting infrastructures and providers. In Figure 5.5, the service life cycle manager handles the service management at run time by interacting with the involved providers. This manager makes recommendations about possible adaptations, substitutions and adjustments of services depending on operational conditions to maintain SLAs according to established contracts between all stakeholders and processes. These contracts are considered during stages 1 to 5 of the service composition process depicted in Figure 5.5. These steps, not shown at this stage, will receive special attention in a future dedicated document.

[Annex E](annex.md#annex-e---service-composition-example) provides a practical "service composition model example" involving multiple cloud providers, services and infrastructures that can be accomplished using currently available cloud services.
