# Gaia-X Conceptual Model

## Main Concepts

### Gaia-X ecosystems

[Gaia-X Credentials](operating_model.md#gaia-x-credentials-and-attestations) provide the necessary trust elements to create ecosystems that operate under a commonly defined governance. The Gaia-X credentials ensure that the policy rules agreed upon between the participants are verified and can be validated at any time.

Components of such ecosystems typically are:

- the ecosystem governance: defining the set of rules agreed upon by the parties in the ecosystem - which must be operationalised. The ecosystem governance typically defines a set of providers, who are providing services to enable basic ecosystem services (like trust services, catalogues -- see chapter [Enabling and Federation Services](enabling_services.md)) which we define as "Federation Services".
- infrastructure - i.e., hardware and software for computing, storage, and network services - adopting the rules defined by the governance. This includes services that support a federation of providers ("Federation Services")
- data ecosystems and data spaces, where participants adopt the governance, using the infrastructures "to access and use data in a fair, transparent, proportionate and/non-discriminatory manner with clear and trustworthy data governance mechanisms." [^semicdataspace]. Data Spaces can span across several Infrastructure and Data Ecosystems.

[^semicdataspace]: <https://joinup.ec.europa.eu/collection/semic-support-centre/data-spaces>

The Gaia-X Ecosystem is the virtual set of Service Offerings described by Gaia-X compliant credentials, according to the Compliance schemes set by the Gaia-X Association.


### Decentralised trust framework for ecosystems

In this challenging environment where each Data Space wants to both be interoperable and yet [adapt](operating_model.md#extention-by-the-ecosystems) their governance to their vertical, domain-specific needs, local market regulation, the Gaia-X Trust Framework provides a set of world-wide applicable rules and specifications usable by:

- the ecosystem governance (e.g.: Data Spaces authorities, such as Data Intermediaries from the [Data Governance Act](https://digital-strategy.ec.europa.eu/en/policies/data-governance-act-explained)).
- ecosystems seeking interoperability and technical compatibility of their services.

The ecosystem governance defines the applicable policy rules to participate in the digital (infrastructure, data or services) ecosystem, together with the Trust Anchors and Schema Extensions policy rules which apply to operators of services in the specific ecosystem.

The interoperability in terms of governance is assessed by the [Gaia-X Compliance](https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/) and the [Trust Indexes](enabling_services.md#trust-indexes).

The interoperability in terms of technical compatibility is assessed by the 
[Gaia-X Testbed](annex.md#annex-b---testbed).

![Gaia-X Ecosystem](figures/gaiax_ecosystem.svg)
*Figure 3.1 - The Gaia-X Trust Framework as a foundation for interoperable data spaces*

#### NIST Cloud Federation Reference Architecture

The three planes from the above picture can be mapped on the three planes described in the NIST Cloud Federation Reference Architecture [chapter 2](https://doi.org/10.6028/NIST.SP.500-332):

- The Trust plane: this plane represents the global digital governance that is shared across Data Spaces and Federations.
- The Management plane: this plane represents an extension of the common digital governance to answer specific business needs.
- The Usage plane: this plane captures the technical interoperability, including the one between Service Offerings and Products.

### The Gaia-X model building block

The building block of the Gaia-X model is based on policy expressions attached to each entity of the model.
The `policies` are expressed by one or more `parties` about one or more `assets`.
An `asset` can also be a `party`, making this simple model recursive and capable of handling the most complicated user scenario, with multiple providers, consumers, federators, [data intermediaries](https://digital-strategy.ec.europa.eu/en/policies/data-governance-act-explained#ecl-inpage-l4ihlqt9), data subjects, business legal representatives, employer/employee and much more.

```mermaid
flowchart TD
    party(Party)
    policy(Policy)
    rule(Rule)
    asset(Asset)
    action(Action)

%%    party -- expresses --> policy
    policy -- a non-empty set of --> rule
    rule -- ability/inability/obligation to exercise --> action
    action -- is an operation on --> asset
    asset -- subjects to --> rule
    party -- has role in --> rule
    asset -. can also be a .-> party
```

The workflow above is the generic representation of the [Open Digital Rights Language (ODRL)](https://www.w3.org/TR/odrl-model/) model, which is used in the rest of this document.

## Roles


### Trust Anchor

Gaia-X Trust Anchors are bodies, parties, i.e., Conformity Assessment Bodies or technical means [accredited](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.7) by the bodies of the Gaia-X Association to be parties eligible to issue attestations about specific claims.


### Participant

A Participant is an entity, as defined in ISO/IEC 24760-1 as an "item
relevant for the purpose of operation of a [domain](https://www.iso.org/obp/ui/#iso:std:iso-iec:24760:-1:ed-2:v1:en)  that has recognisably distinct existence"[^7], which is onboarded and has a Gaia-X Participant Credential. A Participant can take on one or more of the following roles: Provider, Consumer, and Operator. 

Provider and Consumer represent the core roles that are in a business-to-business relationship, while Operators are  Providers that have the specific role of providing Federation Services, enabling the interaction between Providers and Consumers.

[^7]: ISO/IEC. IT Security and Privacy — A framework for identity management: Part 1: Terminology and concepts (24760-1:2019(en)). ISO/IEC. <https://www.iso.org/obp/ui/#iso:std:iso-iec:24760:-1:ed-2:v1:en>

#### Provider

A Provider operates Resources in the Gaia-X Ecosystem and offers them as services through Gaia-X Service Offering credentials. For any such service, the Gaia-X Provider defines the Service Offering including terms and conditions as well as technical policies. Furthermore, it provides the Service Instance that includes a Credential and associated policies. A Gaia-X Provider is responsible for conformity to the claims made. If third-party data, services or infrastructure are part of the service offerings the Gaia-X Provider can make those resources available (e.g., through Service Composition) but remains responsible and shall ensure coverage through appropriate back-to-back coverage.

#### Operator

Operators are Gaia-X Providers that have been approved by the ecosystem governance to operate Federation Services and the Federation, which are independent of each other. There can be one or more Operators per type of Federation Service. 

#### Consumer

A Consumer is a Participant who searches Service Offerings and consumes Service Instances in the Gaia-X Ecosystem to enable digital offerings for End-Users.

### Basic Interactions of Participants

This section describes the basic interaction of the different
[Participants](gx_conceptual_model.md#participant) in an ecosystem based on the Gaia-X model.

Providers and Consumers within the ecosystem are identified and well
described through their valid Credentials, which are initially
created before or during the onboarding process. Providers define their
Service Offerings
and publish them in a Catalogue. In turn, Consumers
search for Service Offerings in Gaia-X Catalogues that are coordinated
by Operators and the Gaia-X Registry. Once the Consumer finds a matching Service Offering in a
Gaia-X Catalogue, the Contract negotiation between Provider and Consumer
determines further conditions under which the Service Instance will be
provided. The Gaia-X Association does not play an intermediary role during the
Contract negotiations but ensures the trustworthiness of all relevant Participants
and Service Offerings.

The following diagram presents the general workflow for Gaia-X service
provisioning and consumption processes. Please note that this overview represents
the current situation and may be subject to changes. The technical specifications will provide more
details about the different elements that are part of the concrete
processes.

![Basic Provisioning and Consumption Process](figures/Simple_Diagram.drawio.svg
)

*Figure 3.2 - Basic processes for provisioning and consumption of Gaia-X services*

## Components

### Gaia-X Credentials

Gaia-X Credentials - formerly known as Self-Descriptions (*SD*) - are cryptographically signed attestations describing Entities from the Gaia-X Conceptual Model in a machine-interpretable format.

The Gaia-X Credentials are the building blocks of a decentralized machine-readable knowledge graph of claims, each credential carrying a tamper-proof and authenticable part of the information of that graph.

The knowledge graph can be completed and extended by Federations and Data Spaces, which always keep control of what and how much information is being shared by implementing access control at the credentials level.

```mermaid
flowchart LR
    description(a description)
    credential(a credential)
    description -- once cryptographically signed is a --> credential
```

Gaia-X Credentials are [W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model-2.0/#credentials) containing claims using the Gaia-X schema in their [context](https://www.w3.org/TR/json-ld11/#the-context). Claims are expressed in [RDF](https://www.w3.org/TR/rdf11-primer/).

Other types of credentials or credentials not using the Gaia-X schema are out of the scope of Gaia-X.

Format of Gaia-X Credentials is defined in [Identity, Credential and Access Management Document](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/22.10/credential_format/) 

#### Gaia-X schema

The Gaia-X members define the Schema for Gaia-X Credentials. It is used as the vocabulary of the claims about credential subjects and must be available in the form of SHACL shapes (cf. the W3C Shapes Constraint Language SHACL[^shacl]).

[^shacl]: <https://www.w3.org/TR/shacl/>

At any point where Credentials are created or received, a certain set of SHACL shapes is known, which forms a shapes graph. A Credential forms a data graph. For compliance with Gaia-X and/or specific ecosystem extensions, this data graph must be validated against the given shapes graph according to the SHACL specification.

The defined version of the Gaia-X Schema is maintained and made available through the 
[Gaia-X Registry Service](https://registry.gaia-x.eu).

### Policies

The main aspects to be considered from a technical point of view are:

- Policy representation: interoperable access and usage policies that are specified in a human- and machine-readable format. Policies generally express three possible restrictions: prohibitions, obligations, and permissions. Constraints defining a rule can be combined into more complex rules, which then form the applicable policy. 
- Decision-taking/policy engine: during the execution of a data transaction, the policies need to be evaluated. This decision typically requires context information. With the decision context, the policy engine will decide whether the request or usage is permitted. 
The evaluation process is handled by the policy engine, which is instantiated by the data product provider and the data consumer or a trusted third party.
- Enforcement and execution of policies: the enforcement and execution of policies is a key capability which needs to be implemented for both access and usage policies. 



Realization of Policy Definition and Policy Enforcement follows the [W3C ODRL specifications](https://www.w3.org/TR/odrl-model/); the definition of the execution components follows NIST (see [PDP](https://csrc.nist.gov/glossary/term/policy_decision_point) and [PEP](https://csrc.nist.gov/glossary/term/policy_enforcement_point))

#### Gaia-X Policy Reasoning Engine

The Gaia-X Policy Reasoning Engine allows for a comparison between policies set up by the provider of a service and usage intentions declared by a consumer, by leveraging the following standards:

- W3C ODRL (Open Digital Rights Language) to express policies 
- W3C Verifiable Credentials 
- JSON Path to evaluate the credentials 
- RDF to represent the policies as triples (subject, predicate, object)
- SPARQL to query the RDF triples.  

An open source library to perform policy reasoning is being provided by the Gaia-X Lab.

#### Policy Decision Point (PDP)

##### Reference implementation

A specific [ODRL profile](https://gitlab.com/gaia-x/lab/policy-reasoning/odrl-vc-profile) has been built by the Gaia-X Lab with the purpose to be able to refer in a clear and precise way to verifiable credential claims in an ODRL Policy. This gives assignors of policies a way to enforce policies using trustworthy and verifiable claims from an assignee, and in doing so having more trust and confidence in the enforcement of the policy.

#### Policy Enforcement 

The Policy Enforcement Point (PEP) intercepts the request from the data product provider. For verification of the request, the decision context is set up and processed through the Policy Decision Point (PDP). 
After retrieving the relevant context information, the data request is verified. If the request is invalid, the data product provider sends a rejection that is processed by the data consumer. If the request is valid and certain actions are required, these are executed before granting data access or starting the transfer. The response is processed by the Data Consumer. If the request is valid, the same interception and processing steps as on the Data Product Provider side are executed (involvement of PAP, PDP, PEP, PIP).
In the last phase, the data consumer provides proof of the implemented policy enforcement that the data product provider verifies. In case of an invalid proof, the data product provider can and must reject the interaction and may initiate further actions. If everything is valid the transaction is completed.
 
The enforcement phase starts when the actual data transaction is being executed and it takes place throughout the data transaction. The goal of the enforcement phase is to evaluate the relevant rules of the policy and decide whether or not the data transaction is allowed to proceed unless an agreement or contract has already been negotiated, in which case the only need is to verify the contract's validity. 

For the different types of rules, the evaluation might occur at different stages of the data transaction:
- Access rules are primarily evaluated by the data provider before any data is exchanged. A data consumer may also check these rules when receiving data to ensure it is still according to the access rules.
- Usage rules are evaluated by the data consumer when the data is used. Depending on the usage rule, evaluation might be required not just when starting to use the data but constantly throughout the lifecycle of the data usage.
- Consent rules require the evaluation of consent of third parties, which might be revoked during the data transaction or data use. Therefore, evaluation should occur both at the data product provider and consumer sides.

![Policy Enforcement flow part 1](figures/Policies_Enforcement_original_part1.jpg)

![Policy Enforcement flow part 2](figures/Policies_Enforcement_original_part2.jpg)
*Figure 3.3 - Policy Enforcement Flow*

### External


#### Identities

An Identity is composed of a unique Identifier and an attribute or set of attributes that uniquely describe an entity within a given context. Gaia-X uses existing Identities and does not maintain them directly.

Identities uniquely describe participants (natural persons, companies) and resources (e.g., machines, interconnection or data endpoints). Personal and Corporate Identities are validated by the Gaia-X Compliance Service and Gaia-X Participant credentials issued. Identities are represented using [**Party Credential**](https://gaia-x.gitlab.io/technical-committee/federation-services/icam/party_credential/) specializations.


#### Services

All offerings by Gaia-X providers are considered "Services" (these can be composed of different types of physical or virtual resources). A service description that follows the Gaia-X Schema and whose claims are validated by the Gaia-X Compliance Service becomes a Gaia-X Service-Offering Credential.


## Overview picture

![Conceptual Model Overview](figures/gaiax-Conceptual_Model.svg)
*Figure 3.4 - Gaia-X conceptual model overview*
