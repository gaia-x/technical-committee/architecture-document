# Glossary & References

## Glossary
The Gaia-X online glossary is at https://gaia-x.gitlab.io/glossary/

## References

- Berners-Lee, T. (2009). Linked Data. W3C. <https://www.w3.org/DesignIssues/LinkedData>

- BITKOM (2022). Data Mesh – Datenpotenziale finden und nutzen. <https://www.bitkom.org/sites/main/files/2022-06/220531_LF_Data_Mesh.pdf>

- Bohn, R. B., Lee, C. A., & Michel, M. (2020). The NIST Cloud Federation Reference Architecture: Special Publication (NIST SP) - 500-332. NIST Pubs. <https://doi.org/10.6028/NIST.SP.500-332>

- Dehghani, Z. (2022). Data Mesh. Delivering Data-Driven Value at Scale. Sebastopol, CA: O’Reilly

- ETSI. Network Functions Virtualisation (NFV). <https://www.etsi.org/technologies/nfv>

- European Commission. Trusted List Browser: Tool to browse the national eIDAS Trusted Lists and the EU List of eIDAS Trusted Lists (LOTL). <https://webgate.ec.europa.eu/tl-browser/#/>

- European Commission. (2020). Towards a next generation cloud for Europe. <https://ec.europa.eu/digital-single-market/en/news/towards-next-generation-cloud-europe>

- European Commission Semantic Interoperability Community. DCAT Application Profile for data portals in Europe. <https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/solution/dcat-application-profile-data-portals-europe>

- Evans, E. (2004). Domain-Driven Design. Tackling complexity in the heart of software. Upper Saddle River, NJ: Addison-Wesley.

- Federal Ministry for Economic Affairs and Energy. (2019). Project Gaia-X: A Federated Data Infrastructure as the Cradle of a Vibrant European Ecosystem. <https://www.bmwi.de/Redaktion/EN/Publikationen/Digitale-Welt/project-Gaia-X.htm>

- Federal Ministry for Economic Affairs and Energy. (2020). Gaia-X: Technical Architecture: Release - June, 2020. <https://www.data-infrastructure.eu/GAIAX/Redaktion/EN/Publications/Gaia-X-technical-architecture.html>

- Gaia-X Association. Architecture Decision Record (ADR) Process: GitLab Wiki. <https://gitlab.com/Gaia-X/Gaia-X-technical-committee/Gaia-X-core-document-technical-concept-architecture/-/wikis/home>

- ISO / IEC. Intelligent transport systems - Using web services (machine-machine delivery) for ITS service delivery (ISO / TR 24097-3:2019(en)). <https://www.iso.org/obp/ui/fr/#iso:std:iso:tr:24097:-3:ed-1:v1:en>

- ISO / IEC. IT Security and Privacy – A framework for identity management: Part 1: Terminology and concepts (24760-1:2019(en)). ISO / IEC. <https://www.iso.org/obp/ui/#iso:std:iso-iec:24760:-1:ed-2:v1:en>

- IX-API. IX-API. <https://ix-api.net/>

- OASIS (2013). Topology and Orchestration Specification for Cloud Applications Version 1.0. <http://docs.oasis-open.org/tosca/TOSCA/v1.0/TOSCA-v1.0.html>

- Oldehoeft, A. E. (1992). Foundations of a security policy for use of the National Research and Educational Network. Gaithersburg, MD. NIST. <https://doi.org/10.6028/NIST.IR.4734 https://doi.org/10.6028/NIST.IR.4734>

- Open Source Initiative. Licenses & Standards. <https://opensource.org/licenses>

- Open Source Initiative. The Open Source Definition (Annotated). <https://opensource.org/osd-annotated>

- Machado, I., Costa, C., & Yasmina Santos, M. (2022). Data Mesh: Concepts and Principles of a Paradigm Shift in Data Architectures. Procedia Computer Science 196, 263–271

- Platform Industrie 4.0: Working Group on the Security of Networked Systems. (2016). Technical Overview: Secure Identities. <https://www.plattform-i40.de/PI40/Redaktion/EN/Downloads/Publikation/secure-identities.pdf> Singhal, A., Winograd, T., & Scarfone, K. A. (2007). Guide to secure web services: Guide to Secure Web Services - Recommendations of the National Institute of Standards and Technology. Gaithersburg, MD. NIST. <https://csrc.nist.gov/publications/detail/sp/800-95/final https://doi.org/10.6028/NIST.SP.800-95>

- W3C. JSON-LD 1.1: A JSON-based Serialization for Linked Data [W3C Recommendation 16 July 2020]. <https://www.w3.org/TR/json-ld11/>

- W3C. ODRL Information Model 2.2 [W3C Recommendation 15 February 2018]. <https://www.w3.org/TR/odrl-model/>

- W3C. Verifiable Credentials Data Model 1.0: Expressing verifiable information on the Web [W3C Recommendation 19 November 2019]. <https://www.w3.org/TR/vc-data-model/>

- W3C. (2015). Semantic Web. <https://www.w3.org/standards/semanticweb/>

- W3C. (2021). Decentralized Identifiers (DIDs) v1.0. <https://www.w3.org/TR/did-core/>
